package application;

import logic.Board;

public class Drawer {
	
	private Board board;
	private Tetris field;
	
	public Drawer(Board board, Tetris field) {
		setBoard(board);
		setField(field);
	}

	public void relocateBlock() {
		
	};
	
	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public Tetris getField() {
		return field;
	}

	public void setField(Tetris field) {
		this.field = field;
	}
	
}
