package application;

import java.util.Timer;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import logic.Board;
import logic.GameLoop;

public class Tetris extends Application{
	
	private GridPane gameField;
	private FlowPane root;
	private Label score;
	private Timer timer;
	private Board board;
	private GameLoop gl;
	private Timeline gameLoop;
	
	@Override
	public void init() throws Exception {
		// TODO Auto-generated method stub
		super.init();
		
		
		timer = new Timer();
		root = new FlowPane();
		gameField = new GridPane();
		boardInit(gameField);
		root.getChildren().add(gameField);
		board = new Board(gameField);
		board.genTet();
		gl = new GameLoop(board);
		gameLoop = new Timeline(new KeyFrame(Duration.seconds(0.45),gl));
	}
	
	public void start(Stage primaryStage) {
		try {
			score = new Label("Score: ");
			root.getChildren().add(score);
			Scene scene = new Scene(root,400,500);
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			
			board.spawnTet();
			
			gameLoop.setCycleCount(Timeline.INDEFINITE);
			gameLoop.play();
//			timer.schedule(new GameLoop(board), 0, 700);
			
		} 
		
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void boardInit(GridPane board) {
		
		for(int i=0;i<10;i++) {
			for(int j=0;j<22;j++) {
				Rectangle rectangle1 = new Rectangle();
                rectangle1.setHeight(20);
                rectangle1.setWidth(20);
                rectangle1.setFill(Color.WHITE);
                rectangle1.setStrokeWidth(0.7);
                rectangle1.setStroke(Color.LIGHTGREY);
                board.add(rectangle1, i, j);
			}
		}
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
}
