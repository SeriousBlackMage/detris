package Tetriminos;

import javafx.scene.paint.Color;

public class L extends Tetrimino {

	public L() {
		setRotation(0);
		setBlockId("L");
		setBlocks(Batrix.L.north);
		setColor(Color.BLUE);
	}
	
	@Override
	public void Rotate() {
		// TODO Auto-generated method stub
		switch(getRotation()) {
		case 0:
			setBlocks(Batrix.L.east);
			setRotation(1);
			break;
		case 1:
			setBlocks(Batrix.L.south);
			setRotation(2);
			break;
		case 2:
			setBlocks(Batrix.L.west);
			setRotation(3);
			break;
		case 3:
			setBlocks(Batrix.L.north);
			setRotation(0);
		}
	}

}
