package Tetriminos;

import javafx.scene.paint.Color;

public class Nrev extends Tetrimino {

	
	public Nrev() {
		setRotation(0);
		setBlockId("Nrev");
		setBlocks(Batrix.Nrev.north);
		setColor(Color.GREEN);
	}
	
	@Override
	public void Rotate() {
		// TODO Auto-generated method stub
		switch(getRotation()) {
		case 0:
			setBlocks(Batrix.Nrev.east);
			setRotation(1);
			break;
		case 1:
			setBlocks(Batrix.Nrev.south);
			setRotation(2);
			break;
		case 2:
			setBlocks(Batrix.Nrev.west);
			setRotation(3);
			break;
		case 3:
			setBlocks(Batrix.Nrev.north);
			setRotation(0);
		}
	}

}
