package Tetriminos;

import javafx.scene.paint.Color;

public class Lrev extends Tetrimino {

	
	public Lrev() {
		setRotation(0);
		setBlockId("Lrev");
		setBlocks(Batrix.Lrev.north);
		setColor(Color.DARKORANGE);
	}
	
	@Override
	public void Rotate() {
		// TODO Auto-generated method stub
		switch(getRotation()) {
		case 0:
			setBlocks(Batrix.Lrev.east);
			setRotation(1);
			break;
		case 1:
			setBlocks(Batrix.Lrev.south);
			setRotation(2);
			break;
		case 2:
			setBlocks(Batrix.Lrev.west);
			setRotation(3);
			break;
		case 3:
			setBlocks(Batrix.Lrev.north);
			setRotation(0);
		}
	}

}
