package Tetriminos;

import javafx.scene.paint.Color;

public class I extends Tetrimino {

	
	public I() {
		setRotation(0);
		setBlockId("I");
		setBlocks(Batrix.I.north);
		setColor(Color.ALICEBLUE);
	}
	
	@Override
	public void Rotate() {
		// TODO Auto-generated method stub
		switch(getRotation()) {
		case 0:
			setBlocks(Batrix.I.east);
			setRotation(1);
			break;
		case 1:
			setBlocks(Batrix.I.south);
			setRotation(2);
			break;
		case 2:
			setBlocks(Batrix.I.west);
			setRotation(3);
			break;
		case 3:
			setBlocks(Batrix.I.north);
			setRotation(0);
		}
	}

}
