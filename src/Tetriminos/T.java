package Tetriminos;

import javafx.scene.paint.Color;

public class T extends Tetrimino {

	
	public T() {
		setRotation(0);
		setBlockId("T");
		setBlocks(Batrix.T.north);
		setColor(Color.FUCHSIA);
	}
	
	@Override
	public void Rotate() {
		// TODO Auto-generated method stub
		switch(getRotation()) {
		case 0:
			setBlocks(Batrix.T.east);
			setRotation(1);
			break;
		case 1:
			setBlocks(Batrix.T.south);
			setRotation(2);
			break;
		case 2:
			setBlocks(Batrix.T.west);
			setRotation(3);
			break;
		case 3:
			setBlocks(Batrix.T.north);
			setRotation(0);
		}
	}

}
