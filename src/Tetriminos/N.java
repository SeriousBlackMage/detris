package Tetriminos;

import javafx.scene.paint.Color;

public class N extends Tetrimino {

	
	public N() {
		setRotation(0);
		setBlockId("N");
		setBlocks(Batrix.N.north);
		setColor(Color.RED);
	}
	
	@Override
	public void Rotate() {
		// TODO Auto-generated method stub
		switch(getRotation()) {
		case 0:
			setBlocks(Batrix.N.east);
			setRotation(1);
			break;
		case 1:
			setBlocks(Batrix.N.south);
			setRotation(2);
			break;
		case 2:
			setBlocks(Batrix.N.west);
			setRotation(3);
			break;
		case 3:
			setBlocks(Batrix.N.north);
			setRotation(0);
		}
	}

}
