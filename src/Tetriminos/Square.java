package Tetriminos;

import javafx.scene.paint.Color;

public class Square extends Tetrimino {

	
	public Square() {
		setRotation(0);
		setBlockId("Square");
		setBlocks(Batrix.I.north);
		setColor(Color.YELLOW);
	}
	
	@Override
	public void Rotate() {
		// TODO Auto-generated method stub
		switch(getRotation()) {
		case 0:
			setBlocks(Batrix.Square.east);
			setRotation(1);
			break;
		case 1:
			setBlocks(Batrix.Square.south);
			setRotation(2);
			break;
		case 2:
			setBlocks(Batrix.Square.west);
			setRotation(3);
			break;
		case 3:
			setBlocks(Batrix.Square.north);
			setRotation(0);
		}
	}

}
