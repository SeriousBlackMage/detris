package logic;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class GridBlock extends Rectangle {
	public GridBlock() {
		setHeight(20);
		setWidth(20);
		setFill(Color.WHITE);
		setStrokeWidth(0.7);
		setStroke(Color.LIGHTGREY);
	}
}
