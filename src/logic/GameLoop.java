package logic;

import application.Drawer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class GameLoop implements EventHandler<ActionEvent>{
private Drawer dr;
private Board br;

public GameLoop(Board br) {
	setBr(br);
}

public Board getBr() {
	return br;
}

public void setBr(Board br) {
	this.br = br;
}

public Drawer getDr() {
	return dr;
}

public void setDr(Drawer dr) {
	this.dr = dr;
}

@Override
public void handle(ActionEvent arg0) {
	{
		// TODO Auto-generated method stub
		
//		br.spawnBlock();
		
		System.out.println("relocate");
		br.relocateTet();
	}
}
}
