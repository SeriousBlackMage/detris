package logic;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Block extends Rectangle {
	private int blockX;
	private int blockY;
	
	public Block(Color fillColor) {
		setStrokeWidth(1.9);
        setStroke(Color.BLACK);
        setFill(fillColor);
        setHeight(20);
        setWidth(20);
	}
	
	public int getBlockX() {
		return blockX;
	}
	public void setBlockX(int blockX) {
		this.blockX = blockX;
	}
	public int getBlockY() {
		return blockY;
	}
	public void setBlockY(int blockY) {
		this.blockY = blockY;
	}
	
	
	
}
