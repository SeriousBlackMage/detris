package logic;

import java.util.Random;

import Tetriminos.*;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Board {

	private Tetrimino curTet;
	private Tetrimino nextTet;
	private int TetX;
	private int TetY;
	private GridPane board;
	private int[][] gesetzt;
	private Block[] curBlocks;
	
	public Board(GridPane board) {
		setBoard(board);
		curBlocks = new Block[4];
		initGesetzt();
	}
	
	public void initGesetzt() {
		gesetzt = new int[22][10];
		for(int i=0;i<22;i++) {
			for(int j=0;j<10;j++) {
				gesetzt[i][j] = 0;
			}
		}
	}
	
	public void spawnTet() {
		genTet();
		int num = 0;
		for (int x = 0; x < 5; x++) {
            for (int y = 0; y < 5; y++) {
                if (curTet.getBlocks()[x][y] > 0) {
                    Block bl = new Block(curTet.getColor());
                    board.add(bl, (x+3), (y-1));
                    bl.setBlockX((x+3));
                    bl.setBlockY((y-1));
                    curBlocks[num] = bl;
                    num++;
                }
            }
            
		}
		
	}
	
	public void relocateTet() {
		
		for(int i=0;i<4;i++) {
			int x = curBlocks[i].getBlockX();
			int y = curBlocks[i].getBlockY();
			
			board.add(curBlocks[i], x, y+1);
			curBlocks[i].setBlockY((y+1));
			
			Rectangle rectangle1 = new Rectangle();
            rectangle1.setHeight(20);
            rectangle1.setWidth(20);
            rectangle1.setFill(Color.WHITE);
            rectangle1.setStrokeWidth(0.7);
            rectangle1.setStroke(Color.LIGHTGREY);
			board.add(rectangle1,x,y);
		}
		
	}
	
	public void clearLine(int lineNum) {
		
	}
	
	public int clearLineNum() {
		for(int[] i:gesetzt) {
			for(int num:i) {
				
			}
		}
		return -1;
	}
	
	public Tetrimino getCurTet() {
		return curTet;
	}


	public void setCurTet(Tetrimino curTet) {
		this.curTet = curTet;
	}


	public Tetrimino getNextTet() {
		return nextTet;
	}


	public void setNextTet(Tetrimino nextTet) {
		this.nextTet = nextTet;
	}


	public int getTetX() {
		return TetX;
	}


	public void setTetX(int TetX) {
		this.TetX = TetX;
	}


	public int getTetY() {
		return TetY;
	}


	public void setTetY(int TetY) {
		this.TetY = TetY;
	}


	public GridPane getBoard() {
		return board;
	}


	public void setBoard(GridPane board) {
		this.board = board;
	}


	public int[][] getGesetzt() {
		return gesetzt;
	}


	public void setGesetzt(int[][] gesetzt) {
		this.gesetzt = gesetzt;
	}


	public void genTet() {
		int ranNum = new Random().nextInt(6);
		switch(ranNum) {
		case 0:
			curTet = new I();
			break;
		case 1:
			curTet = new L();
			break;
		case 2:
			curTet = new Lrev();
			break;
		case 3:
			curTet = new N();
			break;
		case 4:
			curTet = new Nrev();
			break;
		case 5:
			curTet = new Square();
			break;
		case 6:
			curTet = new T();
			break;
		}
	}
	
}
